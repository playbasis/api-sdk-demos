// Helpers
const urlParams = new URLSearchParams(location.search);
const userDefinedPlayerId = urlParams.get('player');  // https://demo1.pbapp.net/?player=demoplayer
const randomPlayerId = "player-" + Math.floor(Math.random() * 1000000);

// Configuration
var cfg_baseUrl = "https://drgn-api.pbapp.net";
var cfg_baseAsyncUrl = "https://api.pbapp.net/async/call";
var cfg_apiKey = "3167969483";
var cfg_apiSecret = "ee9156cd936c40f441644bb92b8fdd68";
var cfg_playerId = userDefinedPlayerId ? userDefinedPlayerId : randomPlayerId;
var cfg_baseEmail = "@playbasis.com";
var cfg_isRandomId = userDefinedPlayerId ? false : true;