const messages = document.getElementById("messages") as HTMLUListElement;

const participants = document.getElementById("participants") as HTMLInputElement;

const rewardPosition = document.getElementById("rewardPosition") as HTMLInputElement;
rewardPosition.oninput = () => {
    calcualteRewardPosition();
}

const rewardPositionSelectionPosition : HTMLElement = document.getElementById("rewardPositionSelection");
calcualteRewardPosition();

const validator = document.getElementById("validator") as HTMLButtonElement;
validator.onclick = () => {
    validate();
}

function validate() {
    console.log('1');
}

function calcualteRewardPosition() {
    processRewardPosition(parseInt(rewardPosition.value));
}

function clearRewardPosition() {
    rewardPositionSelectionPosition.innerHTML = '';
}

function clearRewardPositionSelection() {
    rewardPositionSelectionPosition.innerHTML = '';
}


function clearMessages() {
    messages.innerHTML = '';
}

function addMessage(message: string) {
    const li = document.createElement('li') as HTMLLIElement;
    li.innerHTML = message;
    messages.append(li);
}

function processRewardPosition(positions: number) {
    clearMessages();
    const max = parseInt(participants.value);
    if (positions > max) {
        addMessage('Reward Position (' + positions + ') cannot be greater than Participants (' + max + ')');
        positions = max
        buildPrizeRatio(positions)
    } else {
        buildPrizeRatio(positions)
    }
}

const rangeLabels = [
    '1st',
    '2nd',
    '3rd',
    '4th',
]

const order = ['th', 'st', 'nd', 'rd'];
function rangeLabel(n: number) : string {
    return n + '' + ["th","st","nd","rd"][Math.abs(~[1,2,3].indexOf(+(+n).toFixed().substr(-1)))];
}

function buildPrizeRatio(positions: number) {
    clearRewardPositionSelection();
    for(let i = 0; i < positions; i++) {
        const ele = document.createElement('select') as HTMLSelectElement;
        for (let c = 100;  c > 0;) {
            const option = document.createElement('option') as HTMLOptionElement;
            option.value = c + '';
            option.text = rangeLabel(i + 1) + ' Prize Ratio = ' + c + '%';
            ele.appendChild(option);
            c -= 5
        }
        rewardPositionSelectionPosition.appendChild(ele);
    }
}

export class UI {
    hello() {
        console.log('hello')
    }
}