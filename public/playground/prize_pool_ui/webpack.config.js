const path = require('path');
const { CleanWebpackPlugin } = require('./clean-webpack-plugin');
const HtmlWebpackPlugin = require('./html-webpack-plugin');

const name = 'PrizePool'
const SRC_DIR = path.resolve(__dirname, "./src/");

const _module = {
  rules: [
    {
      test: /\.tsx?$/,
      use: 'ts-loader',
      include: SRC_DIR,
      exclude: /node_modules/
    }
  ]
};

const _resolve = {
  extensions: [ '.tsx', '.ts', '.js' ]
};

const serverConfig = {
  target: 'node',
  output: {
    path: path.resolve(__dirname, 'dist.node'),
    filename: name + '.js'
  },
  plugins: [
    new CleanWebpackPlugin(),
  ],
  module: _module,
  resolve: _resolve
};

const clientConfig = {
  target: 'web',
  output: {
    filename: name + '.[contenthash].js',
    path: path.resolve(__dirname, 'dist.browser')
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
        title: 'Conceptual Modeling for Prize Pool',
        template: 'src/index.html',
        minify: {
          collapseWhitespace: true
        }
    })
  ],
  module: _module,
  resolve: _resolve
};

module.exports = [ serverConfig, clientConfig ];
