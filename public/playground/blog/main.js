const API_ENDPOINT = 'https://demo-api.pbapp.net'
const API_KEY = '2094297024'
const API_SECRET = '511a3dfcbd70c07417668c58f8d81ddd'

Vue.prototype.$Playbasis = Playbasis
//import Vue2Filters from 'vue2-filters'

Playbasis
    .builder
    .setEndpoint(API_ENDPOINT)
    .setApiKey(API_KEY)
    .setApiSecret(API_SECRET)
    .build()

Vue.use(SemanticUIVue)
//Vue.use(VueMoment);
import BlogComponent from './BlogComponent.js'
new Vue({
    el: '#app',
    mixins: [Vue2Filters.mixin],
    components: {
        BlogComponent
    },
    data: {
    },
    created() {
    },
    template: `
        <div>
            <h2>BLOG Demo</h2>
            <p>This demo demonstrates the capability of Content API and a simplified Blog mobile site example. The demo is aimed at showing basic blog site features and Channel Subscription among players</p>
            <h4>Features</h4>
            <ol>
                <li>Add new article</li>
                <li>Load articles</li>
            </ol>
            <h4>Pending features</h4>
            <ol>
                <li>Like button and like stat (ContentLike API)</li>
                <li>Pagination for large number of articles (Content API with offset + limit)</li>
                <li>Channel Subscription (Going to review possible solutions)</li>
            </ol>
            <sui-card-group :items-per-row="2" :stackable="true">
                <blog-component title="Test Panel #1"/>
                <blog-component title="Test Panel #2"/>
            </sui-card-group>
        </div>

        `
})