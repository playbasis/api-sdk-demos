
export default {
    props: {
        title: String
    },
    mixins: [Vue2Filters.mixin],
    data() {
        return {
            message: 'Hello Vue!',
            isLoggedOn: false,
            player: null,
            username: null,
            posts: [],
            numOfPosts: 0,
            postTitle: '',
            postSummary: '',
            postMessage: '',
            isShowLoginDialog: false,
            isGuideOn: false,
        }
    },
    methods: {
        getPosts() {
            const options = {
                player_id: this.username,
            }
            this
                .$Playbasis
                .contentApi
                .retrieveContent(options)
                .then((resp) => {
                    if (resp.success) {
                        this.posts = resp.response.result
                        console.log('posts', this.posts)
                    }
                })
        },
        countPosts() {
            const options = {
                player_id: this.username,
            }
            this
                .$Playbasis
                .contentApi
                .countContent(options)
                .then((resp) => {
                    if (resp.success) {
                        this.numOfPosts = resp.response.result
                        console.log('numOfPosts', this.numOfPosts)
                    }
                })
        },
        post() {
            const title = this.postTitle
            const summary = this.postSummary
            const detail = this.postMessage
            const options = {
                player_id: this.username,
                status: true
            }
            this
                .$Playbasis
                .contentApi
                .createContent(title, summary, detail, options)
                .then((resp) => {
                    console.log('contentApi.createContent', resp)
                    if (resp.success) {
                        this.clearForm()
                        this.fetchPosts()
                    }
                    
                })
        }, 
        clearForm() {
            this.postTitle = ''
            this.postSummary = ''
            this.postMessage = ''
        },
        fetchPosts() {
            this.countPosts()
            this.getPosts()
        },
        login(username) {
            this.isShowLoginDialog = false
            this
                .$Playbasis
                .playerApi
                .playerDetailedPublicInfo(username)
                .then((resp) => {
                    console.log(resp)
                    if (resp.success) {
                        this.isLoggedOn = true
                        this.player = resp.response.player
                        this.username = this.player.username
                        this.fetchPosts()
                        console.log('player', this.player)
                        console.log('username', this.username)

                    }
                }, (error) => {
                    const options = {
                        approve_status: 'approved'
                    };
                    Playbasis
                        .playerApi
                        .register(username, username + '@example.com', options)
                        .then((resp) => {
                            console.log('username.register', resp);
                            if (resp.success) {
                                this.login(username)
                            }
                        });

                   

                })
        }
    },
    created() {
        this.username = 'player' + Date.now()
    },
    template: `
            <sui-card style="vertical-align: text-top;">
                <sui-card-content>
                <sui-image v-if="player" :src="player.image" class="right floated" width="200"/>
                <sui-card-header v-if="isLoggedOn">{{username | uppercase}}</sui-card-header>
                <sui-card-meta>{{title}}  
                <br><br>
                    <sui-checkbox v-if="player" label="Design Guide" toggle v-model="isGuideOn"/>
                    <br><br>
                    <sui-button @click="isShowLoginDialog = true" circular icon="sign-in" v-if="!player">SignIn</sui-button>
                    <sui-button @click="isShowLoginDialog = true" circular icon="sign-out" v-if="player">SignOut</sui-button>
                </sui-card-meta>
                <sui-card-description></sui-card-description>

                <fieldset v-if="player"> 
                    <legend v-show="isGuideOn">Playbasis.contentApi.countContent() | <a href="google.com" target="_blank">Documentation</a></legend>
                    <sui-form onsubmit="return false;">
                        <sui-form-field>
                            <input type="text" v-if="isLoggedOn" placeholder="title" v-model="postTitle"/>
                        </sui-form-field>
                        <sui-form-field>
                            <textarea v-if="isLoggedOn" placeholder="summary" v-model="postSummary" rows="2"/>
                        </sui-form-field>
                        <sui-form-field>
                            <textarea v-if="isLoggedOn" placeholder="messages" v-model="postMessage"/>
                        </sui-form-field>
                        <sui-button @click="post" circular v-if="player && isLoggedOn">
                            Post
                        </sui-button>
                    </sui-form>
                </fieldset>

                <sui-feed style="padding: 20px;">
                    <sui-feed-event v-for="post in orderBy(posts, 'date_added', -1)" :key="post._id">
                        <sui-feed-label :image="player.image" />
                        <sui-feed-content>
                            <sui-feed-summary>
                                <a>{{username}}</a> <sui-feed-date>Undefined posted time</sui-feed-date>
                            </sui-feed-summary>
                            <sui-feed-extra text>
                                <fieldset><legend v-show="isGuideOn">content.title</legend>
                                <b>{{post.title}}</b>
                                </fieldset>
                                <fieldset><legend v-show="isGuideOn">content.summary</legend>
                                <p><i>"{{post.summary}}"</i></p>
                                </fieldset>
                                <fieldset><legend v-show="isGuideOn">content.detail</legend>
                                <p>{{post.detail}}</p>
                                </fieldset>
                            </sui-feed-extra>
                        </sui-feed-content>
                    </sui-feed-event>
                </sui-feed>

                </sui-card-content>

                <sui-modal v-model="isShowLoginDialog" size="tiny">
                    <sui-modal-header>Login demo player</sui-modal-header>
                    <sui-modal-content image>
                        <sui-modal-description>
                            <sui-form onsubmit="return false;">
                                <sui-form-field>
                                    <label>Username</label>
                                    <input placeholder="Username" v-model="username">
                                </sui-form-field>
                            </sui-form>
                        </sui-modal-description>
                    </sui-modal-content>
                    <sui-modal-actions>
                        <sui-button circular @click="login('demoplayer')">
                        Login with Demo player
                        </sui-button>
                        <sui-button circular @click="login(username)">
                        Login & Register if not exist
                        </sui-button>
                    </sui-modal-actions>
                </sui-modal>

                

            </sui-card>
        `
}