
const UI_PLAYER = document.getElementById('PLAYER');
const UI_PENDING_QUIZZES = document.getElementById('PENDING_QUIZZES');
const UI_COMPLETED_QUIZZES = document.getElementById('COMPLETED_QUIZZES');
const UI_QUIZ_PENDING_PLAYERS = document.getElementById('QUIZ_PENDING_PLAYERS');
const UI_QUIZ_COMPLETED_PLAYERS = document.getElementById('QUIZ_COMPLETED_PLAYERS');

var appStates = {
    system: {
        GAME_MANAGER_PLAYER_ID: 'QUIZ_GAME_MANAGER',
        QUIZ_PENDING_PLAYERS_NODE_ID: '5d3158b47d10acff188b4567',
        QUIZ_COMPLETED_PLAYERS_NODE_ID: '5d3158c07d10ac01198b4568',
        DEMO_QUIZ_ID: '5d12d4a3ec2a2e576e8b4570',
        API_ENDPOINT: 'https://drgn-api.pbapp.net',
        API_KEY: '3167969483',
        API_SECRET: 'ee9156cd936c40f441644bb92b8fdd68'
    },
    session: {
        quizPendingPlayers: [],
        quizCompletedPlayers: []
    },
    player: {
        id: null,
        loggedOn: null,
        total_max_score: 0,
        total_score: 0
    }
};

Playbasis.builder
    .setEndpoint(appStates.system.API_ENDPOINT)
    .setApiKey(appStates.system.API_KEY)
    .setApiSecret(appStates.system.API_SECRET)
    .build();

function countAnswerStat(answerId, increaseEnabled, callback) {
    Playbasis
        .contentApi
        .retrieveContent({ title: answerId, player_id: appStates.system.GAME_MANAGER_PLAYER_ID })
        .then((data) => {
            //console.log('retrieveContent', data);
            const contents = data.response.result;
            var FOUND_ANSWER_ID = contents.length > 0
            if (FOUND_ANSWER_ID) {
                const content = contents[0];
                const currentCount = parseInt(content.summary);
                if (increaseEnabled) {
                    const newCount = currentCount + 1;
                    Playbasis
                        .contentApi
                        .updateContent(content.node_id, { player_id: appStates.system.GAME_MANAGER_PLAYER_ID, title: answerId, summary: '' + newCount, detail: '' + newCount })
                        .then((data) => {
                            //console.log('updateContent', data);
                            if (data.success) {
                                if (callback) callback(newCount);
                            } else {
                                if (callback) callback(-1);
                            }
                        }, (error) => {
                            console.log('error1', error)
                            if (callback) callback(-1);
                        });
                } else {
                    if (callback) callback(currentCount);
                }
            } else {
                // Must use '0 ' (zero + 1 space)
                Playbasis
                    .contentApi
                    .createContent(answerId, '0 ', '0 ', {
                        player_id: appStates.system.GAME_MANAGER_PLAYER_ID,
                        status: true
                    })
                    .then((data) => {
                        //console.log('createContent', data);
                        if (data.success) {
                            if (callback) callback(0);
                        } else {
                            if (callback) callback(-1);
                        }
                    }, (error) => {
                        console.log('error2', error)
                        if (callback) callback(-1);
                    });
            }
        });
}

function init(callback) {
    $('#PROGRESS').hide();
    var inputPlayerId = prompt("Please enter your name", "clarkson");
    console.log('inputPlayerId', inputPlayerId)
    if (inputPlayerId && inputPlayerId != '' && inputPlayerId.length > 0) {
        isValidPlayerId(inputPlayerId, (isValid) => {
            if (isValid) {
                console.log('isValid', isValid)
                appStates.player.id = inputPlayerId;
                UI_PLAYER.innerHTML = 'Welcome ' + appStates.player.id;
                appStates.player.loggedOn = new Date();
                resetQuiz(fetchAll);
                if (callback) callback();
            } else {
                console.log('isValid', isValid)
                createNewPlayer(inputPlayerId, () => {
                    appStates.player.id = inputPlayerId;
                    UI_PLAYER.innerHTML = 'Welcome ' + appStates.player.id;
                    appStates.player.loggedOn = new Date();
                    resetQuiz(fetchAll);
                    if (callback) callback();
                });
            }
        });
    } else {
        ons.notification.alert('Player name required');
    }
}

function isJoinedChallenge() {
    const pendingPlayersIncludingMe = appStates.session.quizPendingPlayers.filter((p) => {
        return p == appStates.player.id
    });
    return pendingPlayersIncludingMe.length > 0 ? true : false;
}

function isValidPlayerId(playerId, callback) {
    Playbasis
        .playerApi
        .playerInfo(playerId)
        .then((data) => {
            console.log('playerInfo', data);
            callback(data.success);
        }, (error) => {
            callback(false);
        });
}

function createNewPlayer(playerId, callback) {
    const options = {
        password: '123456789',
        approve_status: 'approved' // approved, rejected, pending
    };
    Playbasis
        .playerApi
        .register(playerId, playerId + '@example.com', options)
        .then((data) => {
            console.log('createNewPlayer', data);
            callback(data.success);
        }, (error) => {
            callback(false);
        });
}

function getQuestionFromQuiz(quizId, callback) {
    Playbasis
        .quizApi
        .getQuestionFromQuiz(quizId, appStates.player.id)
        .then((data) => {
            console.log('getQuestionFromQuiz', data);
            callback(data.response.result);
        });
}

function listPendingQuizzes(callback) {
    if (isJoinedChallenge()) {
        $('#PROGRESS').show();
        UI_PENDING_QUIZZES.innerHTML = '';
        Playbasis
            .quizApi
            .listOfActiveQuizzes()
            .then((data) => {
                console.log('listOfActiveQuizzes', data);
                const quizzes = data.response.result;
                quizzes.forEach((quiz) => {
                    getQuestionFromQuiz(quiz.quiz_id, (singleQuestion) => {
                        if (singleQuestion) {
                            UI_PENDING_QUIZZES.innerHTML = '<h1>' + singleQuestion.question + '</h1>';
                            singleQuestion.options.forEach((option) => {
                                const statLabelId = 'answer_' + option.option_id + '_stat';
                                loadAnswerStat(option.option_id, (answerCount) => {
                                    showAnswerStat(statLabelId, answerCount);
                                })
                                UI_PENDING_QUIZZES.innerHTML +=
                                    '<button class="button button--outline" style="margin: 20px;display: block;" onclick="submitAnswer(\'' + quiz.quiz_id + '\', \'' + singleQuestion.question_id + '\', \'' + option.option_id + '\',\'' + singleQuestion.question + '\',\'' + option.description + '\')">' + option.description + ' - answered <span id="' + statLabelId + '"></span></button>';
                            });
                        }

                        $('#PROGRESS').hide();
                    });
                });
                
                if (callback) callback(quizzes);
            }, (error) => {
                $('#PROGRESS').hide();
            });
    }

}

function showAnswerStat(statLabelId, count) {
    const ui = document.getElementById(statLabelId);
    if (ui) {
        ui.innerHTML = count;
    };
}

function loadAnswerStat(optionId, callback) {
    if (callback) {
        const increaseEnabled = false;
        countAnswerStat('answer_' + optionId, increaseEnabled, (count) => {
            callback(count);
        });
    }
}

function listCompletedQuizzes() {
    UI_COMPLETED_QUIZZES.innerHTML = '';
    Playbasis
        .quizApi
        .listQuizDone(appStates.player.id)
        .then((data) => {
            const quizzes = data.response.result;
            const quizzesUI = quizzes.map((quiz) => {
                return '<b>Game Completed SCORE: ' + appStates.player.total_score + '/' + appStates.player.total_max_score + '</b> <ons-button onclick="unjoin()">Play again</ons-button>';
            }).join("");
            UI_COMPLETED_QUIZZES.innerHTML = quizzesUI;
        }, (error) => {});
}

function resetQuiz(callback) {
    appStates.session.quizPendingPlayers = [];
    appStates.session.quizCompletedPlayers = [];
    UI_PENDING_QUIZZES.innerHTML = 'Click join button to start';
    UI_COMPLETED_QUIZZES.innerHTML = '';
    Playbasis
        .quizApi
        .resetQuiz(appStates.player.id, appStates.system.DEMO_QUIZ_ID)
        .then((data) => {
            console.log('resetQuiz', data);
            if (data.success) {
                Playbasis
                    .storeOrganizeApi
                    .removePlayerFromNode(appStates.system.QUIZ_PENDING_PLAYERS_NODE_ID, appStates.player.id)
                    .then(() => {
                        Playbasis
                            .storeOrganizeApi
                            .removePlayerFromNode(appStates.system.QUIZ_COMPLETED_PLAYERS_NODE_ID, appStates.player.id)
                            .then(callback, callback);
                    }, () => {
                        Playbasis
                            .storeOrganizeApi
                            .removePlayerFromNode(appStates.system.QUIZ_COMPLETED_PLAYERS_NODE_ID, appStates.player.id)
                            .then(callback, callback);
                    });
            }
        }, fetchAll);
}

function submitAnswer(quizId, questionId, optionId, questionText, answerText) {
    if (isJoinedChallenge()) {
        Playbasis
            .quizApi
            .answerQuestion(quizId, appStates.player.id, questionId, optionId, 'answer')
            .then((data) => {
                //console.log('answerQuestion', data);
                if (data.success) {
                    const increaseEnabled = true;
                    countAnswerStat('answer_' + optionId, increaseEnabled);

                    const action = data.response.result.score == 0 ? 'answerwrong' : 'answercorrect';
                    Playbasis
                        .engineApi
                        .rule(action, appStates.player.id, {
                            playerId: appStates.player.id,
                            datetime: new Date(),
                            question: questionText,
                            answer: answerText,
                        })
                        .then((data) => {
                            console.log('ACTION ' + action, data);
                        });

                    if (data.response.result.is_last_question) {
                        console.log('is_last_question', data);
                        appStates.player.total_max_score = data.response.result.total_max_score;
                        appStates.player.total_score = data.response.result.total_score;

                        UI_PENDING_QUIZZES.innerHTML = 'Click play again button to start';

                        Playbasis
                            .engineApi
                            .rule('complete-quiz', appStates.player.id, {
                                playerId: appStates.player.id,
                                datetime: new Date(),
                                totalScore: appStates.player.total_score,
                                totalMaxScore: appStates.player.total_max_score
                            })
                            .then((data) => {
                                console.log('ACTION complete-quiz', data);
                            });


                        Playbasis
                            .storeOrganizeApi
                            .addPlayerToNode(appStates.system.QUIZ_COMPLETED_PLAYERS_NODE_ID, appStates.player.id)
                            .then(() => {
                                UI_PENDING_QUIZZES.innerHTML = '';
                                Playbasis
                                    .storeOrganizeApi
                                    .removePlayerFromNode(appStates.system.QUIZ_PENDING_PLAYERS_NODE_ID, appStates.player.id)
                                    .then(fetchAll, (error) => {});
                            });

                    } else {
                        fetchAll();
                    }
                }
            });
    }
}

function fetchAllPlayers() {
    fetchQuizCompletedPlayers();
    fetchQuizPendingPlayers();
}

function fetchAll() {
    fetchQuizCompletedPlayers(() => {
        listCompletedQuizzes();
    });
    fetchQuizPendingPlayers(() => {
        listPendingQuizzes();
    });
}

function join() {
    resetQuiz(() => {
        Playbasis
            .storeOrganizeApi
            .removePlayerFromNode(appStates.system.QUIZ_COMPLETED_PLAYERS_NODE_ID, appStates.player.id)
            .then(activePlayer, activePlayer);
    });
}

function unjoin() {
    console.log('unjoin');
    Playbasis
        .storeOrganizeApi
        .removePlayerFromNode(appStates.system.QUIZ_PENDING_PLAYERS_NODE_ID, appStates.player.id)
        .then(deactivePlayer, deactivePlayer);
}

function activePlayer() {
    Playbasis
        .storeOrganizeApi
        .addPlayerToNode(appStates.system.QUIZ_PENDING_PLAYERS_NODE_ID, appStates.player.id)
        .then((data) => {
            if (data.success) {
                fetchAll();
            }
        }, (error) => {
            fetchAll();
        });
}

function deactivePlayer() {
    Playbasis
        .storeOrganizeApi
        .removePlayerFromNode(appStates.system.QUIZ_COMPLETED_PLAYERS_NODE_ID, appStates.player.id)
        .then((data) => {
            if (data.success) {
                resetQuiz(fetchAllPlayers);
            }
        }, (error) => {
            resetQuiz(fetchAllPlayers);
        });
}

function fetchQuizPendingPlayers(callback) {
    UI_QUIZ_PENDING_PLAYERS.innerHTML = '';
    Playbasis
        .storeOrganizeApi
        .listPlayerFromNode(appStates.system.QUIZ_PENDING_PLAYERS_NODE_ID)
        .then((data) => {
            const players = data.response;
            appStates.session.quizPendingPlayers = players ? players.map((p) => p.player_id) : [];
            UI_QUIZ_PENDING_PLAYERS.innerHTML = appStates.session.quizPendingPlayers.map((playerId) => {
                return '' + playerId + '';
            }).join(', ');
            console.log('quizPendingPlayers', appStates.session.quizPendingPlayers);
            if (callback) callback();
        }, (error) => {
            if (callback) callback();
        });
}

function fetchQuizCompletedPlayers(callback) {
    UI_QUIZ_COMPLETED_PLAYERS.innerHTML = '';
    Playbasis
        .storeOrganizeApi
        .listPlayerFromNode(appStates.system.QUIZ_COMPLETED_PLAYERS_NODE_ID)
        .then((data) => {
            const players = data.response;
            appStates.session.quizCompletedPlayers = players ? players.map((p) => p.player_id) : [];
            UI_QUIZ_COMPLETED_PLAYERS.innerHTML = appStates.session.quizCompletedPlayers.map((playerId) => {
                return playerId;
            }).join(', ');
            console.log('quizCompletedPlayers', appStates.session.quizCompletedPlayers);
            if (callback) callback();
        }, (error) => {
            if (callback) callback();
        });
}