const Playbasis = require('@playbasis1/playbasis')
const config = require('./config')

Playbasis.builder
    .setEndpoint(config.API_ENDPOINT)
    .setApiKey(config.API_KEY)
    .setApiSecret(config.API_SECRET)
    .build();

function getPlayerDetailedInfo(playerId, callback) {
    Playbasis
        .playerApi
        .playerDetailedInfo(playerId)
        .then((resp) => {
            if (callback) callback(resp)
        })
        .catch((err) => {
            console.log('err', err.cause)
        })
}

function getPlayerInfo(playerId, callback) {
    Playbasis
        .playerApi
        .playerInfo(playerId)
        .then((resp) => {
            if (callback) callback(resp)
        })
        .catch((err) => {
            console.log('err', err.cause)
        })
}

/**
 * Register player
 * @param  {String} playerId player id
 * @param  {String} email    email to register player with
 * @param  {String} options  (**optional**) option as Object.  
 * It can include  
 * {  
 * `image`: *String* = url to the player profile image,  
 * `phone_number`: *String* = phone number in format +66xxyyyzzzz,  
 * `facebook_id`: *String* = facebook id of player,  
 * `twitter_id`: *String* = twitter id of player,  
 * `password`: *String* = password,  
 * `first_name`: *String* = first name of player,  
 * `last_name`: *String = last name of player,  
 * `gender`: *Number = 1 for Male | 2 for Female,  
 * `birth_date`: *String* = date of birth in the format YYYY-MM-DD (ex. 1982-09-29),  
 * `code`: *String* = referral code of another player for invitation system,  
 * `anonymous`: *Number* = 0 | 1,  
 * `device_id`: *String* = device id to verify with SMS verification process,  
 * `approve_status`: *String* = "approved" | "rejected" | "pending",  
 * }
 * @return {Object}          Promise Object
 * @method  register
 * @memberof Playbasis.playerApi
 */
function registerPlayer(playerId, email, options, callback) {
    Playbasis
        .playerApi
        .register(playerId, email, options)
        .then((resp) => {
            if (callback) callback(resp)
        })
        .catch((err) => {
            console.log('err', err.cause)
        })
}

function getReferralCode(inviterPlayerId, callback) {
    Playbasis
        .playerApi
        .playerReferralCode(inviterPlayerId)
        .then((resp) => {
            if (callback) callback(resp)
        })
        .catch((err) => {
            console.log('err', err.cause)
        })
}


function referral(inviteePlayerId, inviterReferralCode, callback) {
    playerAPI
        .referral(inviteePlayerId, inviterReferralCode)
        .then((resp) => {
            if (callback) callback(resp)
        })
        .catch((err) => {
            console.log('err', err.cause)
        })
}
        
module.exports = {
    getPlayerInfo: getPlayerInfo,
    getPlayerDetailedInfo: getPlayerDetailedInfo,
    registerPlayer: registerPlayer,
    getReferralCode: getReferralCode,
    referral: referral
}