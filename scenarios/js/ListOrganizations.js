function ListOrganizations() {
  console.log('ListOrganizations start');
  const uiEle = document.getElementById('ORGS');
  uiEle.innerHTML = '';


  Playbasis
      .storeOrganizeApi
      .listOrganizations()
      .then((data) => {
        console.log('listOrganizations', data);
        const orgs = data.response.results;
        if (orgs) {
          orgs.map((o) => {
              const ui = '<li>ORG: ' + o.name + '</li>';
              uiEle.innerHTML += ui;
          });
      }

        console.log('ListOrganizations end');
      }, (error) => {
        console.log('error', error);
        console.log('ListOrganizations end');
      });
}