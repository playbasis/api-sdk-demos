function RegisterPlayer() {
  const options = {
    password: TEST_PLAYER_PASSWORD,
    approve_status: 'approved' // approved (email verification will not be required), rejected, pending
  };
  console.log('RegisterPlayer start');
  Playbasis
      .playerApi
      .register(TEST_PLAYER_ID, TEST_PLAYER_EMAIL, options)
      .then((data) => {
        console.log('data', data);
        console.log('RegisterPlayer end');
      }, (error) => {
        console.log('error', error);
        console.log('RegisterPlayer end');
      });
}