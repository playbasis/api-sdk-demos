function AppAllQuizzes() {
    console.log('AppAllQuizzes start');

    const quizzesEle = document.getElementById('QUIZZES');
    quizzesEle.innerHTML = '';

    Playbasis
        .quizApi
        .listOfActiveQuizzes()
        .then((data) => {
            console.log('data', data);
            const quizzes = data.response.result;
            const quizzesUI = quizzes.map((quiz) => {
                getQuestionFromQuiz(quiz.quiz_id, (singleQuestion) => {
                    if (singleQuestion) {
                        const optionsUI = singleQuestion.options.map((option) => {
                            return '<li><button id="" onclick="answerQuestion(\'' + quiz.quiz_id + '\', \'' + singleQuestion.question_id + '\', \'' + option.option_id + '\')">' + option.description + '</button></li>';
                        }).join("")
                        const questionUI = '<li>QUESTION: ' + singleQuestion.question + '<ul>' + optionsUI + '</ul></li>';
                        quizzesEle.innerHTML += '<li>QUIZ: ' + quiz.description + '<button onclick="resetQuiz(\`' + quiz.quiz_id + '\`)">Reset</button><ul>' + questionUI + '</ul></li>';
                    } else {
                      quizzesEle.innerHTML += '<li>QUIZ: ' + quiz.description + ' <button onclick="resetQuiz(\`' + quiz.quiz_id + '\`)">Reset</button></li>';
                    }

                });
            }).join("");
        }, (error) => {
            console.log('error', error);
            console.log('AppAllQuizzes end');
        });
}

function getQuestionFromQuiz(quizId, callback) {
  const RANDOM = 1;
  const NOT_RANDOM = 2;
    Playbasis
        .quizApi
        .getQuestionFromQuiz(quizId, TEST_PLAYER_ID, {random: NOT_RANDOM})
        .then((data) => {
            console.log('getQuestionFromQuiz', data);
            callback(data.response.result);
        });
}

function resetQuiz(quizId) {
    Playbasis
        .quizApi
        .resetQuiz(TEST_PLAYER_ID, quizId)
        .then((data) => {
            console.log('resetQuiz', data);
            if (data.success) {
              AppAllQuizzes();
              AppAllCompletedQuizzes();
            }
        });
}

function answerQuestion(quizId, questionId, optionId) {
    Playbasis
        .quizApi
        .answerQuestion(quizId, TEST_PLAYER_ID, questionId, optionId, 'answer')
        .then((data) => {
            console.log('answerQuestion', data);
            if (data.response.result.is_last_question) {
              AppAllQuizzes();
              AppAllCompletedQuizzes();
            } else {
              AppAllQuizzes();
            }
        });
}