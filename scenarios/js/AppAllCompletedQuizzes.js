function AppAllCompletedQuizzes() {
  console.log('AppAllCompletedQuizzes start');

  const quizzesEle = document.getElementById('DONE_QUIZZES');
  quizzesEle.innerHTML = '';

  Playbasis
      .quizApi
      .listQuizDone(TEST_PLAYER_ID)
      .then((data) => {
          console.log('listQuizDone', data);

          const quizzes = data.response.result;
          const quizzesUI = quizzes.map((quiz) => {
            return '<li>(Completed)' + quiz.name  + '</li>';
          }).join("");

          quizzesEle.innerHTML = quizzesUI;
          
      }, (error) => {
          console.log('error', error);
          console.log('AppAllCompletedQuizzes end');
      });
}