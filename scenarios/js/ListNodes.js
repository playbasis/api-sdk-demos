function ListNodes() {
    console.log('ListNodes start');

    const uiEle = document.getElementById('NODES');
    uiEle.innerHTML = '';

    Playbasis
        .storeOrganizeApi
        .listNodes()
        .then((data) => {
            console.log('data', data);
            const nodes = data.response.results;
            if (nodes) {
                nodes.map((n) => {
                    const ui = '<li>NODE: ' + n.organize.name + '/' + n.name + ' <button onclick="addMeToNode(\'' + n._id + '\', \'' + TEST_PLAYER_ID + '\')">Add TEST_PLAYER_ID</button> <button onclick="removeMeFromNode(\'' + n._id + '\', \'' + TEST_PLAYER_ID + '\')">Remove TEST_PLAYER_ID</button> <button onclick="listPlayersFromNode(\'' + n._id + '\')">List players</button><ul><li>Players: <ul id="NODE_' + n._id + '_PLAYERS"></ul></li></ul>';
                    uiEle.innerHTML += ui;
                    listPlayersFromNode(n._id);
                });
            }

            console.log('ListNodes end');
        }, (error) => {
            console.log('error', error);
            console.log('ListNodes end');
        });
}

function addMeToNode(nodeId, playerId) {
    Playbasis
        .storeOrganizeApi
        .addPlayerToNode(nodeId, playerId)
        .then((data) => {
            console.log('addMeToNode', data);
            listPlayersFromNode(nodeId);
        });
}

function removeMeFromNode(nodeId, playerId) {
    Playbasis
        .storeOrganizeApi
        .removePlayerFromNode(nodeId, playerId)
        .then((data) => {
            console.log('removeMeFromNode', data);
            listPlayersFromNode(nodeId);
        });
}

function listPlayersFromNode(nodeId) {
    const uiEle = document.getElementById('NODE_' + nodeId + '_PLAYERS');
    uiEle.innerHTML = '';

    Playbasis
        .storeOrganizeApi
        .listPlayerFromNode(nodeId)
        .then((data) => {
            console.log('listPlayersFromNode', data);
            const players = data.response;
            const ui = players.map((p) => {
              return '<li>' + p.player_id + '</li>';
            }).join("");
            uiEle.innerHTML = ui;
        }, (error) => {
          // Dont' care if there are no players added
        });
}