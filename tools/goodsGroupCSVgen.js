
function makeCouponCode(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

const args = process.argv.slice(2);

if (args.length != 6) {
  console.log('USAGE: node goodsGroupCSVgen.js <NumOfCoupon> <CodePrefix> <lengthOfCode> <dateStart in dd-mm-yyyy> <dateEnd in dd-mm-yyyy> <dateExpire in dd-mm-yyyy>')
  process.exit(0)
}

const numOfCoupon = Number(args[0])
const codePrefix = args[1]
const lengthOfCode = Number(args[2])
const dateStart = args[3]
const dateEnd = args[4]
const dateExpire = args[5]
const batchName = 'batch_name_' + codePrefix

var csv = 'coupon_name,coupon_code,date_start,date_end,date_expire,batch_name\n'

for (let n = 0; n < numOfCoupon; n++) {
  csv += 
    codePrefix + '_' + n + ', ' + 
    makeCouponCode(lengthOfCode) + ', ' + 
    dateStart + ' 00:00:00, ' + 
    dateEnd + ' 00:00:00, ' + 
    dateExpire + ' 00:00:00, ' + 
    batchName + 
    '\n'
}

console.log(csv)

