
const request = require("request");
const config = require("./config");

const options = {
    method: 'POST',
    url: config.endpoint + '/Engine/rule',
    headers: {
        'content-type': 'multipart/form-data',
        Accept: 'application/json',
    },
    formData: {
        token: config.token,
        action: 'drgn_transfer_internationalRemittance',
        player_id: config.playerId
    }
};

/* 
DEFAULT ACTIONS
=========================================================================
accept add addon
billpay bookmark buy
checkin cheer click
close-spin comment compare complete
died dislike
finish-invite follow
harvest
invite invited
levelup like listen login logout look love
manual-reward manual-spin
offer-game offer-invite openbox order
pay paybill paybill_for_other play post postvideo
read redeem refer register rent respond-accept respond-decline review run
share shoot spin step subscribe suggest
topup topup_for_other transfer
visit visit-inventory
want watch
*/

request(options, function(error, response, response) {
    if (error) throw new Error(error);
    const json = JSON.parse(response);
    console.log(JSON.stringify(json, null, 2));

    /*
    Successfully registered
    {
        "success": true,
        "error_code": "0000",
        "message": "Success",
        "response": {
            "events": [
            {
                "event_type": "REWARD_RECEIVED",
                "reward_type": "point",
                "value": "10"
            }
            ],
            "events_missions": [],
            "events_quests": [],
            "processing_time": "0.0282"
        },
        "timestamp": 1559031452,
        "time": "Tue, 28 May 2019 15:17:32 +0700 Asia/Bangkok",
        "version": "2.32.0"
    }
    */
});