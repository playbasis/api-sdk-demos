
const request = require("request");
const config = require("./config");

const options = {
    method: 'GET',
    url: config.endpoint + '/Quest?token=' + config.token,
    headers: {
        'content-type': 'multipart/form-data',
        Accept: 'application/json',
    }
};

request(options, function(error, response, response) {
    if (error) throw new Error(error);
    const json = JSON.parse(response);
    console.log(JSON.stringify(json, null, 2));

    /*
    {
        "success": true,
        "error_code": "0000",
        "message": "Success",
        "response": {
            "quests": [
            {
                "quest_name": "SampleQuest",
                "description": "",
                "hint": "",
                "image": "https://images.pbapp.net/no_image.jpg",
                "tags": null,
                "sort_order": "",
                "condition": [
                {
                    "condition_value": "10",
                    "condition_type": "POINT",
                    "condition_id": "52ea1ea78d8c89401c0000b4",
                    "condition_data": {
                    "name": "point"
                    }
                }
                ],
                "rewards": [
                {
                    "reward_value": "1000",
                    "reward_type": "EXP",
                    "reward_id": "52ea1ea78d8c89401c0000b5",
                    "reward_data": {
                    "name": "exp"
                    }
                }
                ],
                "missions": null,
                "status": true,
                "mission_order": false,
                "date_added": "2019-06-05T15:31:33+0700",
                "feedbacks": null,
                "organize_id": null,
                "organize_role": null,
                "date_modified": "2019-06-05T16:19:09+0700",
                "quest_id": "5cf77de52f131cec148b4575"
            }
            ]
        },
        "timestamp": 1559729381,
        "time": "Wed, 05 Jun 2019 17:09:41 +0700 Asia/Bangkok",
        "version": "2.32.0"
        }
        */
});