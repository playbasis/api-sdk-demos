
const request = require("request");
const config = require("./config");

const options = {
    method: 'GET',
    url: config.endpoint + '/Player/' + config.playerId + '?token=' + config.token,
    headers: {
        Accept: 'application/json',
    }
};

request(options, function(error, response, response) {
    if (error) throw new Error(error);
    const json = JSON.parse(response);
    console.log(JSON.stringify(json, null, 2));

    /*
    Successfully registered
    {
        "success": true,
        "error_code": "0000",
        "message": "Success",
        "response": {
            "player": {
            "cl_player_id": "Player1",
            "image": "https://www.pbapp.net/images/default_profile.jpg",
            "username": "Player1",
            "exp": 0,
            "level": 1,
            "first_name": "Player1",
            "last_name": null,
            "gender": 0,
            "tags": null,
            "birth_date": null,
            "registered": "2019-05-28T14:02:56+0700",
            "last_login": "0000-00-00 00:00:00",
            "last_logout": "0000-00-00 00:00:00"
            }
        },
        "timestamp": 1559027235,
        "time": "Tue, 28 May 2019 14:07:15 +0700 Asia/Bangkok",
        "version": "2.32.0"
    }
    */
});