
# Playbasis example

## Runtime configuration

1. Create a new file `config.js`
2. Configure your app detail and some parameters used for learning API

```javascript
module.exports = {
    api_key: 'API_KEY',
    api_secret: 'API_SECRET',
    token: 'APP_TOKEN',
    playerId: 'Player1',
    playerPassword: 'password'
}
```

- `api_key` your app api_key
- `api_secret` your app api_secret
- `token` test token 
- `playerId` test player id
- `playerPassword` test player password

## Run client examples

```shell
npm install
node <ExampleFile>.js
```