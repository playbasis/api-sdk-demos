
const request = require("request");
const config = require("./config");

const options = {
    method: 'GET',
    url: 'https://api.pbapp.net/Player/' + config.playerId + '/badgeAll?token=' + config.token,
    headers: {
        Accept: 'application/json',
    }
};

request(options, function(error, response, response) {
    if (error) throw new Error(error);
    const json = JSON.parse(response);
    console.log(JSON.stringify(json, null, 2));
});