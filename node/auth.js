
const request = require("request");
const config = require("./config");

const options = {
    method: 'POST',
    url: config.endpoint + '/Auth',
    headers: {
        'content-type': 'multipart/form-data',
        Accept: 'application/json',
    },
    formData: {
        api_key: config.api_key,
        api_secret: config.api_secret
    }
};

request(options, function(error, response, response) {
    if (error) throw new Error(error);
    const json = JSON.parse(response);
    console.log(JSON.stringify(json, null, 2));

    /*
    Successfully authenticated
    {
        "success": true,
        "error_code": "0000",
        "message": "Success",
        "response": {
            "token": "TOKEN",
            "date_expire": "2019-05-30T09:50:37+0700"
        },
        "timestamp": 1559025775,
        "time": "Tue, 28 May 2019 13:42:55 +0700 Asia/Bangkok",
        "version": "2.32.0"
    }
    */
});