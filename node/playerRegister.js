
const request = require("request");
const config = require("./config");

const options = {
    method: 'POST',
    url: config.endpoint + '/Auth/player/' + config.playerId + '/register',
    headers: {
        'content-type': 'multipart/form-data',
        Accept: 'application/json',
    },
    formData: {
        api_key: config.api_key,
        api_secret: config.api_secret,
        password: config.playerPassword
    }
};

request(options, function(error, response, response) {
    if (error) throw new Error(error);
    const json = JSON.parse(response);
    console.log(JSON.stringify(json, null, 2));

    /*
    Successfully registered
    {
        "success": true,
        "error_code": "0000",
        "message": "Success",
        "response": [],
        "timestamp": 1559026976,
        "time": "Tue, 28 May 2019 14:02:56 +0700 Asia/Bangkok",
        "version": "2.32.0"
    }

    User already exist
    {
        "success": false,
        "response": null,
        "error_code": "0201",
        "message": "User already exist",
        "timestamp": 1559027025,
        "time": "Tue, 28 May 2019 14:03:45 +0700 Asia/Bangkok",
        "version": "2.32.0"
        }
    */
});