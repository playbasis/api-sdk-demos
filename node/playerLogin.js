
const request = require("request");
const config = require("./config");

const options = {
    method: 'POST',
    url: config.endpoint + '/Player/' + config.playerId + '/login',
    headers: {
        Accept: 'application/json',
    },
    formData: {
        token: config.token
    }
};

request(options, function(error, response, response) {
    if (error) throw new Error(error);
    const json = JSON.parse(response);
    console.log(JSON.stringify(json, null, 2));

    /*
    Successfully login
    {
        "success": true,
        "error_code": "0000",
        "message": "Success",
        "response": [],
        "timestamp": 1559027688,
        "time": "Tue, 28 May 2019 14:14:48 +0700 Asia/Bangkok",
        "version": "2.32.0"
    }
    */
});